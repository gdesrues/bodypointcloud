# Reconstruct a surface from point cloud

See the [vtkSurfaceReconstructionFilter](https://vtk.org/doc/nightly/html/classvtkSurfaceReconstructionFilter.html) class.


![](point_cloud_body.png)



## Or use the given file

https://github.com/leonid-pishchulin/humanshape/blob/master/fitting/facesShapeModel.mat


Example:
https://gitlab.inria.fr/gdesrues1/bodypointcloud/-/blob/master/buildsurface.py

![](triangulated.png)