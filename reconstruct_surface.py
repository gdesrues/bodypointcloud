import logging

import numpy as np
import treefiles as tf
from MeshObject import Mesh


@tf.timer
def main():
    root = tf.f(__file__)
    root.file(a="point_cloud.vtk", b="reconstructed_surface.vtk")

    m = Mesh.load(root.a)
    m.SurfaceReconstructionFilter(SetSampleSpacing=5, SetNeighborhoodSize=7)
    m.threshold((-15, 15), "ImageScalars")
    m.TriangleFilter()
    m.ConnectivityFilter(SetExtractionModeToAllRegions=[], ColorRegionsOn=[])
    m.threshold((0, 0), "RegionId")
    m.smooth(relax=0.2)
    m.triangulate(m.mmg_options(hmax=20, hmin=20, hgrad=1, nr=True))
    m.make_manifold()
    m.write(root.b)

    with tf.PvPlot() as p:
        p.camera_position = [
            (2562.965808205884, -3354.0620145109465, 2348.893411214769),
            (-17.000783538119023, 174.47472115733936, 969.4630104611747),
            (-0.15647696377556278, 0.2582732478790428, 0.9533152098008256),
        ]
        p.add_mesh(Mesh.load(root.b), style="wireframe", color="blue")
        p.add_pts(Mesh.load(root.a).pts, s=7)
        p.show_bounds()


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
