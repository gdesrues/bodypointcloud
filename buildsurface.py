import logging

import numpy as np
import treefiles as tf
from MeshObject import Mesh, Plotter, Themes
import scipy.io


def main():
    m = Mesh.load("point_cloud.vtk")
    mc = m.copy()

    cells = scipy.io.loadmat("facesShapeModel.mat")["faces"]
    mc.cells = cells - np.ones_like(cells)
    mc.filterCellsTo(Mesh.TRIANGLE)

    with Plotter(theme=Themes.DOC) as p:
        p.add_points(m, point_size=5, color="b")
        p.add_mesh(mc)
        p.camera_position = [
            (3.0500024414059532, -3262.4435178328945, 889.6179999999996),
            (3.0500024414062636, 154.54399993896484, 889.6179999999999),
            (-1.100117804202999e-32, -1.2111249889822818e-16, 1.0),
        ]
        p.render()
        p.screenshot("triangulated.png")


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
